# Framework #

### Paquetes ###
> * "league/container":  Contenedor de php league
> * "league/route": Routeador
> * "illuminate/database": ORM ELOQUENT
> * "twig/twig":  Motor de templates
> * "aura/session":   Sessiones
> * "zendframework/zend-diactoros": generar dispatch de la aplicacion  mediante request y response
> * "kint-php/kint":  debug de la info
> * "vlucas/phpdotenv":  permite el uso de archivos .env
