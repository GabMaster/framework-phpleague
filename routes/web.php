<?php
//$strategy = (new \League\Route\Strategy\ApplicationStrategy)->setContainer($container);
//$route = (new \League\Route\Router)->setStrategy($strategy);
  $route = new \League\Route\RouteCollection($container);



$route->group('/', function (\League\Route\RouteGroup $route) use($container){
    $route->map('GET', '/', 'App\Controllers\HomeController::index');
});

return $route;